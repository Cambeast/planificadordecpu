/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vista;

import java.util.Scanner;
import javax.swing.JOptionPane;
import logica.Proceso;

/**
 *
 * @author ivond
 */
public class PlanificadordeCPU {

    public static void main(String[] args) {
        
        int ll, cpu, prioridad = 0;
        int nro_procesos;
        int tipo_planificacion;
       
       do{
            tipo_planificacion = Integer.parseInt(JOptionPane.showInputDialog("Ingrese el tipo de Planificacion: \n[1]FIFO [2]SJF [3]Prioridad (No expropiativo) [4]SRTF "));
            if(tipo_planificacion < 1 || tipo_planificacion > 4){
                JOptionPane.showMessageDialog(null, "Numero de planifiacion ERRONEO","ERROR!!", JOptionPane.ERROR_MESSAGE);} 
       }while(tipo_planificacion < 1 || tipo_planificacion > 4);
        
       do{
            nro_procesos = Integer.parseInt(JOptionPane.showInputDialog("Ingrese el nro de procesos a planificar: "));
            if(nro_procesos < 4 || nro_procesos > 8){
                JOptionPane.showMessageDialog(null, "Numero de procesos ERRONEO","ERROR!!", JOptionPane.ERROR_MESSAGE);} 
       }while(nro_procesos < 4 || nro_procesos > 8);
     
       Proceso proceso[] = new Proceso[nro_procesos];
       
           JOptionPane.showMessageDialog(null, "Ingrese la informacion de procesos:");
           for(int i=0; i< nro_procesos; i++) { 
                
                ll = Integer.parseInt(JOptionPane.showInputDialog("Ingrese el nro llegada del proceso " + (i+1)));
                cpu = Integer.parseInt(JOptionPane.showInputDialog("Ingrese el tiempo de cpu del proceso " + (i+1)));
                
                if(tipo_planificacion == 3){
                    prioridad = Integer.parseInt(JOptionPane.showInputDialog("Ingrese el nro de prioridad del proceso " + (i+1)));
                }
                
                Proceso elproceso = new Proceso(i+1, ll, cpu , 0, 0, 0, prioridad);
                proceso[i] = elproceso;
                
           }
       
       switch (tipo_planificacion) {
           case 1: 
              fifo(proceso, nro_procesos);
              break;
           case 2:
               sjf(proceso, nro_procesos);
               break;
           case 3:
               prioridad(proceso, nro_procesos);
               break;
           case 4:
               srtfP(proceso, nro_procesos);
               break;
       }
    }

    private static void fifo(Proceso[] proceso, int n) {
        int menor = 0;
        int nummenor;
        int tiempo = 0;
        int com, fin, esp, temp;
        boolean swapped;
        
        for (int i = 0; i < n; i++) 
        {
            swapped = false;
            for (int j = 0; j < n - i - 1; j++) 
            {
                if (proceso[j].getT_llegada() > proceso[j + 1].getT_llegada()) 
                {
                    
                    temp = proceso[j].getT_llegada();
                    proceso[j].setT_llegada(proceso[j + 1].getT_llegada());
                    proceso[j + 1].setT_llegada(temp);

                    
                    temp = proceso[j].getT_cpu();
                    proceso[j].setT_cpu(proceso[j+1].getT_cpu());
                    proceso[j + 1].setT_cpu(temp);

                    
                    temp = proceso[j].getNu();
                    proceso[j].setNu(proceso[j+1].getNu());
                    proceso[j + 1].setNu(temp);

                    
                    swapped = true;
                }
            }
            if (swapped == false) 
            {
                break;
            }
        }
           
            for(int i=0; i < n; i++){
              
                nummenor = proceso[menor].getT_llegada();
                
                if( nummenor < proceso[i].getT_llegada()){
                    menor = i; 
                }
             
                if(tiempo < proceso[menor].getT_llegada()){
                   com = proceso[menor].getT_llegada();
                }else{
                   com = tiempo;  
                } 
                  fin = proceso[menor].getT_cpu() + tiempo;
                  esp = com - proceso[menor].getT_llegada();
                    
                  Proceso elproceso = new Proceso(proceso[menor].getNu(),proceso[menor].getT_llegada(), proceso[menor].getT_cpu(), com, fin, esp, 0);
                  proceso[menor] = elproceso;
                  tiempo += proceso[menor].getT_cpu();
        }
        System.out.println("\t\tFIFO"); 
       mostrartabla(proceso,n);
    }

    private static void sjf(Proceso[] proceso, int n) {
        int nummenor;
        int tiempo = 0;
        int com, fin, esp, temp;
        boolean swapped;
        
        for (int i = 0; i < n; i++) 
        {
            swapped = false;
            for (int j = 0; j < n - i - 1; j++) 
            {
                if (proceso[j].getT_llegada() > proceso[j + 1].getT_llegada()) 
                {
                    
                    temp = proceso[j].getT_llegada();
                    proceso[j].setT_llegada(proceso[j + 1].getT_llegada());
                    proceso[j + 1].setT_llegada(temp);

                    
                    temp = proceso[j].getT_cpu();
                    proceso[j].setT_cpu(proceso[j+1].getT_cpu());
                    proceso[j + 1].setT_cpu(temp);

                    
                    temp = proceso[j].getNu();
                    proceso[j].setNu(proceso[j+1].getNu());
                    proceso[j + 1].setNu(temp);

                    
                    swapped = true;
                }
            }
            if (swapped == false) 
            {
                break;
            }
        }
        
        com = proceso[0].getT_llegada();
        fin = proceso[0].getT_cpu() + tiempo;
        esp = com - proceso[0].getT_llegada();
        Proceso elproceso = new Proceso(proceso[0].getNu(),proceso[0].getT_llegada(), proceso[0].getT_cpu(), com, fin, esp, 0);
        proceso[0] = elproceso;
        tiempo += proceso[0].getT_cpu();
        
        for(int i = 1; i < n; i++){
            swapped = false;
            for (int j = 1; j < n - i; j++) 
            {
                if (proceso[j].getT_cpu() > proceso[j + 1].getT_cpu()) 
                {
                    
                    temp = proceso[j].getT_llegada();
                    proceso[j].setT_llegada(proceso[j + 1].getT_llegada());
                    proceso[j + 1].setT_llegada(temp);

                    
                    temp = proceso[j].getT_cpu();
                    proceso[j].setT_cpu(proceso[j+1].getT_cpu());
                    proceso[j + 1].setT_cpu(temp);

                    
                    temp = proceso[j].getNu();
                    proceso[j].setNu(proceso[j+1].getNu());
                    proceso[j + 1].setNu(temp);

                    
                    swapped = true;
                }
            }
            if (swapped == false) 
            {
                break;
            }
        }
        
        for(int i=1; i < n; i++){
              
                nummenor = proceso[i].getT_cpu();
             
                if(tiempo < proceso[i].getT_llegada()){
                   com = proceso[i].getT_llegada();
                }else{
                   com = tiempo;  
                } 
                  fin = proceso[i].getT_cpu() + tiempo;
                  
                  if(proceso[i].getT_llegada()>=fin){
                      com = proceso[i].getT_llegada();
                      fin = proceso[i].getT_llegada() + proceso[i].getT_cpu(); 
                  }
                  esp = com - proceso[i].getT_llegada();
 
                  elproceso = new Proceso(proceso[i].getNu(),proceso[i].getT_llegada(), proceso[i].getT_cpu(), com, fin, esp, 0);
                  proceso[i] = elproceso;
                  tiempo += proceso[i].getT_cpu();
        }
       System.out.println("\t\tSJF"); 
       mostrartabla(proceso,n);
    }

    private static void prioridad(Proceso[] proceso, int n) {
        int menor = 1;
        int nummenor;
        int tiempo = 0;
        int com, fin, esp, temp;
        boolean swapped;
        
        for (int i = 0; i < n; i++) 
        {
            swapped = false;
            for (int j = 0; j < n - i - 1; j++) 
            {
                if (proceso[j].getT_llegada() > proceso[j + 1].getT_llegada()) 
                {
                    
                    temp = proceso[j].getT_llegada();
                    proceso[j].setT_llegada(proceso[j + 1].getT_llegada());
                    proceso[j + 1].setT_llegada(temp);

                    
                    temp = proceso[j].getT_cpu();
                    proceso[j].setT_cpu(proceso[j+1].getT_cpu());
                    proceso[j + 1].setT_cpu(temp);

                    
                    temp = proceso[j].getNu();
                    proceso[j].setNu(proceso[j+1].getNu());
                    proceso[j + 1].setNu(temp);

                    swapped = true;
                }
            }
            if (swapped == false) 
            {
                break;
            }
        }
        
        com = proceso[0].getT_llegada();
        fin = proceso[0].getT_cpu() + tiempo;
        esp = com - proceso[0].getT_llegada();
        Proceso elproceso = new Proceso(proceso[0].getNu(),proceso[0].getT_llegada(), proceso[0].getT_cpu(), com, fin, esp, proceso[0].getPrioridad());
        proceso[0] = elproceso;
        tiempo += proceso[0].getT_cpu();
        
        for(int i = 1; i < n; i++){
            swapped = false;
            for (int j = 1; j < n - i ; j++) 
            {
                if (proceso[j].getPrioridad() > proceso[j + 1].getPrioridad()) 
                {
                    
                    temp = proceso[j].getT_llegada();
                    proceso[j].setT_llegada(proceso[j + 1].getT_llegada());
                    proceso[j + 1].setT_llegada(temp);

                    
                    temp = proceso[j].getT_cpu();
                    proceso[j].setT_cpu(proceso[j+1].getT_cpu());
                    proceso[j + 1].setT_cpu(temp);

                    
                    temp = proceso[j].getNu();
                    proceso[j].setNu(proceso[j+1].getNu());
                    proceso[j + 1].setNu(temp);
                    
                    temp = proceso[j].getPrioridad();
                    proceso[j].setPrioridad(proceso[j+1].getPrioridad());
                    proceso[j + 1].setPrioridad(temp);
                    
                    swapped = true;
                }
            }
            if (swapped == false) 
            {
                break;
            }
        }
        
        for(int i=1; i < n; i++){
              
                nummenor = proceso[i].getPrioridad();
             
                if(tiempo < proceso[i].getPrioridad()){
                   com = proceso[i].getT_cpu();
                }else{
                   com = tiempo;  
                } 
                  fin = proceso[i].getT_cpu() + tiempo;
                  
                  if(proceso[i].getT_llegada()>fin){
                      com = proceso[i].getT_llegada();
                      fin = proceso[i].getT_llegada() + proceso[i].getT_cpu(); 
                  }
                  esp = com - proceso[i].getT_llegada();
 
                  elproceso = new Proceso(proceso[i].getNu(),proceso[i].getT_llegada(), proceso[i].getT_cpu(), com, fin, esp, proceso[i].getPrioridad());
                  proceso[i] = elproceso;
                  tiempo += proceso[i].getT_cpu();
        }
        
        
        mostrartabla(proceso,n);
    }

    private static void srtfP(Proceso[] proceso, int n) {
        
         for(int s=0; s < n; s++){
            
        }
    }
    
    private static void mostrartabla(Proceso[] proceso, int n){
        if(proceso[0].getPrioridad() == 0){
            System.out.println("Nro.\tT.lleg\tT.CPU\tT.Com\tT.Fin\t T.Esp");
        }else{
            System.out.println("Nro.\tT.lleg\tT.CPU\tPrioridad\tT.Com\tT.Fin\t T.Esp");
        }
        
        for(int i=0; i < n; i++){
            for(int j=0; j < n; j++){
                if(proceso[j].getNu() == (i+1)){
                    System.out.print(proceso[j]);
                    System.out.print("\t");
                    System.out.println();
                }
            }
        }
    }
}
